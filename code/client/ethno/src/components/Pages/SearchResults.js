import React, { useEffect, useState } from "react";
import apiService from "../../services/api/api";
import { useLocation, useNavigate } from "react-router-dom";
import AttractionNotFound from "./AttractionNotFound";
import CardList from "../List/CardList";

const SearchResults = () => {
  const navigate = useNavigate();
  const listType = "attractionInfoFromSearch"
  const location = useLocation();
  let attractions;
  const [listOfAttractionInfo, setListOfAttractionInfo] = useState([]);

  useEffect(() => {
    attractions = location.state.attractionsList;
    if (attractions.length > 0) {
      createAttractionsInfoList().catch(console.error);
    }
  }, []);

  const getAttractionInfo = async attraction => {
    return await apiService.AttractionService.getAttractions(attraction.id);
  };

  const createAttractionsInfoList = async () => {
    const listOfAttractionInfoPromiseObject = attractions.map(
      getAttractionInfo
    );

    setListOfAttractionInfo(
      await Promise.all(listOfAttractionInfoPromiseObject)
    );
  };

  //__________________________________________________
  // const createAttractionsInfoList = attractions.map(async attraction => {
  //   const listOfAttractionInfo = await apiService.AttractionService.getAttractions(
  //     attraction.attraction_id
  //   ).then(r => {
  //     console.log(r);
  //
  //     console.log("listOfAttractionInfo" + listOfAttractionInfo);
  //   });
  // });
  //__________________________________________________

  //original
  // const getAttractions = attractionsList => {
  //   if (!isNaN(location.state.attractionsList[0])) {
  //      listOfAttractionInfo = attractionsList.map(attraction =>{
  //        await apiService.AttractionService.getAttractions(attraction.attraction_id).then(r =>{
  //          console.log(r);
  //        });
  //      })
  //   }
  // if (!isNaN(location.state.attractionId)) {
  //   await apiService.AttractionService.getAttractions(attractionId).then(
  //     response => {
  //       setAttractionInfo(response);
  //       navigate("/cardPreview", {
  //         state: { attraction: response }
  //       });
  //
  //       console.log(response);
  //     }
  //   );
  // }
  // };
  // if (attractionInfo !== 0) {
  //   return <AttractionInfo attraction={attractionInfo} username={username} />;
  // } else {

  if (listOfAttractionInfo !== undefined && listOfAttractionInfo.length > 0) {
    return <CardList listOfAttractions={listOfAttractionInfo} listType={listType} />;
  } else {
    return <AttractionNotFound />;
  }
};
export default SearchResults;
