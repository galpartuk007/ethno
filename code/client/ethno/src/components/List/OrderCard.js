import React, {useEffect} from "react";
import {useNavigate} from "react-router-dom";
import {Button, Card, Image} from "semantic-ui-react";
import apiService from "../../services/api/api";

const OrderCard = prop => {
    const navigate = useNavigate();
    const orderInfo = prop.data;
    const attraction = orderInfo.availableAttractionID.AttractionID

    //i18n
    const show = "לצפייה באטרקציה";
    const numberOfTickets = "כמות כרטיסים שהוזמנו";
    const attractionDate =  "תאריך";
    const attractionStartTime ="בין השעות";

    let attractionInfo;
    useEffect(() => {
        const fetchData = async () => {
            attractionInfo = await apiService.AttractionService.getAttractions(attraction.id);
        };

        fetchData().catch(console.error);
    }, []);

    const handleClickEditButton = () => {
        navigate("/attractionInfo", {
            state: {attraction: attractionInfo}
        });
    };

    return (
        <>
            <Card fluid color="teal">
                <Card.Content>
                    <Card.Header>
                        {attraction ? attraction.attraction_name : ""}{" "}
                    </Card.Header>
                    <Card.Meta>
                        {attraction ? attraction.Address + ", " + attraction.city : ""}
                    </Card.Meta>
                    <Card.Description>
                        <Image
                            src={attraction ? attraction.bannerImage : ""}
                            floated="right"
                            size="small"
                        />
                        <strong>{attraction ? attraction.description : ""}</strong>
                        <br/>
                        <strong>{attraction ? attraction.summary : ""}</strong>
                        <br/>
                        <strong>{numberOfTickets} : {orderInfo.tickets_number}</strong>
                        <br/>
                        <strong>{attractionDate} : {orderInfo.availableAttractionID.day} , {orderInfo.availableAttractionID.date}</strong>
                        <br/>
                        <strong>{attractionStartTime} : {orderInfo.availableAttractionID.endTime} - {orderInfo.availableAttractionID.startTime}</strong>
                    </Card.Description>
                </Card.Content>
                <Button basic onClick={handleClickEditButton}>
                    {show}
                </Button>
            </Card>
        </>
    );
    return <div>Order</div>;
};

export default OrderCard;
