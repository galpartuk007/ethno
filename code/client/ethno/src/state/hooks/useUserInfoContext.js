import { useCallback, useState } from "react";

const defaultUserInfoState = {
  name: "",
  company: "",
  media: { mediaType: "", url: "" },
  radius: 50,
  regions: 1,
  areas: 1,
  tags: [],
};
export const useUserInfoContext = () => {
  const [userInfoState, setUserInfoState] = useState(defaultUserInfoState);

  const updateUserInfoState = useCallback((updatedUserInfoState) => {
    setUserInfoState((currentUserInfoState) => ({
      ...currentUserInfoState,
      ...updatedUserInfoState,
    }));
  }, []);

  const resetUserInfoState = useCallback(() => {
    setUserInfoState(defaultUserInfoState);
  }, []);

  return { userInfoState, updateUserInfoState, resetUserInfoState };
};
