import { useCallback, useState, useEffect } from "react";

const defaultAuthState = {
  token: "",
  username: "",
};
export const useAuthContext = () => {
  const [authState, setAuthState] = useState(defaultAuthState);

  useEffect(() => {
    updateAuthState({ token: localStorage.getItem("token") || "",username: localStorage.getItem("username") || "" });
  }, []);

  const updateAuthState = useCallback((updatedAuthState) => {
    setAuthState((currentAuthState) => ({
      ...currentAuthState,
      ...updatedAuthState,
    }));
  }, []);

  const resetAuthState = useCallback(() => {
    setAuthState(defaultAuthState);
  }, []);

  return { authState, updateAuthState, resetAuthState };
};
