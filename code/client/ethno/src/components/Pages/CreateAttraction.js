import React, {useEffect, useState} from "react";
import {useEthnoContext} from "../../state/EthnoContextProvider";
import apiService from "../../services/api/api";
import {useNavigate} from "react-router-dom";
import {Button, Container, Dropdown, Form, Modal, TextArea} from "semantic-ui-react";
import TimeField from "react-simple-timefield";

import UploadImage from "../UploadImage";

const CreateAttraction = () => {
    //i18n
    const nameDesc = "שם חוויה";
    const shortDesc = "תיאור קצר";
    const locationDesc = "כתובת";
    const cityDesc = "עיר";
    const durationDesc = "משך סדנא";
    const partOfTheDayDesc = "חלק ביום";
    const cultureDesc = "תרבות";
    const longDesc = "סיכום החוויה";
    const numberOfHours = "שעות";
    const numberOfMinutes = "דקות";
    const ticketPrice = "עלות לכרטיס";
    const kindDesc = "סוג החוויה";
    const bannerDesc = "העלאת תמונת באנר";
    const pictureDesc = "העלאת תמונות של החוויה(מינימום 3)";
    const banner = "banner";
    const picture = "picture";
    const morning = "בוקר";
    const noon = "צהריים";
    const evening = "ערב";
    const send = "שלח";
    const close = "סגור";
    const uploadBanner = "תמונת באנר";
    const uploadImage = "העלאת תמונות";
    const navigate = useNavigate();
    const partOfDayList = [
        {
            key: morning,
            text: morning,
            value: morning
        },
        {
            key: noon,
            text: noon,
            value: noon
        },
        {
            key: evening,
            text: evening,
            value: evening
        }
    ];
    const [bannerImage, setBannerImage] = useState("");
    const [attractionName, setAttractionName] = useState("");
    const [shortDescription, setShorDescription] = useState("");
    const [location, setLocation] = useState("");
    const [city, setCity] = useState("");
    const [kind, setKind] = useState("");
    const [culture, setCulture] = useState("");
    const [partOfDay, setPartOfDay] = useState("");
    const [duration, setDuration] = useState("00:00");
    const [durationHours, setDurationHours] = useState("");
    const [durationMinutes, setDurationMinutes] = useState("");
    const [cost, setCost] = useState(0);
    const [longDescription, setLongDescription] = useState("");
    const [arrayOfPictures, setArrayOfPictures] = useState([]);
    const [enableCreation, setEnableCreation] = useState(true);
    const [openBanner, setOpenBanner] = React.useState(false);
    const [openPicture, setOpenPicture] = React.useState(false);
    const {authState} = useEthnoContext();

    const handleCreateClick = event => {
        event.preventDefault();
        event.target.className = "ui loading button";
        createAttraction();
    };

    const createAttraction = async () => {
        const attractionData = {
            name: attractionName,
            description: shortDescription,
            city: city,
            address: location,
            duration: duration,
            price: cost,
            kind: kind,
            culture: culture,
            partOfDay: partOfDay,
            summary: longDescription,
            bannerImage: bannerImage,
            userName: authState.username,
            images: arrayOfPictures
        };
        apiService.AttractionService.createNewAttraction(attractionData).catch(console.error);
        navigate("/search");
    };

    useEffect(() => {
        if (
            bannerImage != "" &&
            attractionName != "" &&
            shortDescription != "" &&
            location != "" &&
            city != "" &&
            partOfDay != "" &&
            duration != "" &&
            kind != "" &&
            culture != "" &&
            cost != 0 &&
            longDescription != "" &&
            arrayOfPictures.length >= 3
        ) {
            setEnableCreation(false);
        }
    }, [
        bannerImage,
        attractionName,
        partOfDay,
        shortDescription,
        location,
        city,
        duration,
        kind,
        cost,
        culture,
        longDescription,
        arrayOfPictures
    ]);

    const onUploadImage = props => {
        switch (props[0]) {
            case "banner":
                setBannerImage(props[1]);
                break;
            default:
                setArrayOfPictures(arrayOfPictures => [...arrayOfPictures, props[1]]);
        }
    };

    useEffect(() => {
        if (durationHours != "" && durationMinutes != "") {
            setDuration("0" + durationHours + ":" + durationMinutes);
        }
    });

    return (
        <>
            <img src={bannerImage}/>
            <Container text>
                <Form>
                    <Form.Field>
                        <label> {nameDesc}</label>
                        <input
                            type="text"
                            onChange={e => setAttractionName(e.target.value)}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>{kindDesc}</label>
                        <input type="text" onChange={e => setKind(e.target.value)}/>
                    </Form.Field>
                    <Form.Field>
                        <label>{cultureDesc}</label>
                        <input type="text" onChange={e => setCulture(e.target.value)}/>
                    </Form.Field>
                    <Form.Field
                        control={TextArea}
                        placeholder={shortDesc}
                        label={shortDesc}
                        onChange={e => setShorDescription(e.target.value)}
                    />
                    <Form.Field>
                        <label> {locationDesc}</label>
                        <input type="text" onChange={e => setLocation(e.target.value)}/>
                    </Form.Field>
                    <Form.Field>
                        <label> {cityDesc}</label>
                        <input type="text" onChange={e => setCity(e.target.value)}/>
                    </Form.Field>
                    <Form.Field>
                        <label>{durationDesc}</label>
                        <TimeField
                            value={duration}
                            onChange={e => setDuration(e.target.value)}
                            input=<input type="text"/>
                        />
                    </Form.Field>
                    <Form.Field>
                        <label> {partOfTheDayDesc}</label>
                        <Dropdown
                            clearable
                            fluid
                            selection
                            options={partOfDayList}
                            onChange={e => setPartOfDay(e.target.textContent)}
                        />
                    </Form.Field>
                    <Form.Field
                        control={TextArea}
                        placeholder={longDesc}
                        label={longDesc}
                        onChange={e => setLongDescription(e.target.value)}
                    />
                    <Form.Field>
                        <label> {ticketPrice}</label>
                        <input
                            type="number"
                            min="50"
                            max="300"
                            step="10"
                            placeholder={ticketPrice}
                            onChange={e => setCost(parseInt(e.target.value))}
                        />
                    </Form.Field>
                    <Modal
                        onClose={() => setOpenBanner(false)}
                        onOpen={() => setOpenBanner(true)}
                        open={openBanner}
                        trigger={<Button>{uploadBanner}</Button>}
                    >
                        <Modal.Header>Select a Photo</Modal.Header>
                        <Modal.Content>
                            <Modal.Description>
                                <UploadImage onUpload={onUploadImage} type={banner}/>
                            </Modal.Description>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button content={close} onClick={() => setOpenBanner(false)}/>
                        </Modal.Actions>
                    </Modal>
                    <br/> <br/>
                    <Modal
                        onClose={() => setOpenPicture(false)}
                        onOpen={() => setOpenPicture(true)}
                        open={openPicture}
                        trigger={<Button>{uploadImage}</Button>}
                    >
                        <Modal.Header>Select a Photo</Modal.Header>
                        <Modal.Content>
                            <Modal.Description>
                                <UploadImage onUpload={onUploadImage} type={picture}/>
                            </Modal.Description>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button content={close} onClick={() => setOpenPicture(false)}/>
                        </Modal.Actions>
                    </Modal>
                    <Button fluid onClick={handleCreateClick} disabled={enableCreation}>
                        {send}
                    </Button>
                </Form>
            </Container>
        </>
    );
};

export default CreateAttraction;
