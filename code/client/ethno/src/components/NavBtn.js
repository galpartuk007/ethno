import React from "react";
import { Link } from "react-router-dom";

export const NavBtn = ({ path, text, onClick, disabled = false }) => {
  return (
    <Link to={path}>
      <button
        className={"ui button blue"}
        disabled={disabled}
        onClick={onClick}
      >
        {text}
      </button>
    </Link>
  );
};
