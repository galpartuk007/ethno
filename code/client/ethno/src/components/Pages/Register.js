import React, {useEffect, useState} from "react";
import {Button, Container, Form, Modal, TextArea} from "semantic-ui-react";
import apiService from "../../services/api/api";
import {useNavigate} from "react-router-dom";
import UploadImage from "../UploadImage";


const Register = ({}) => {
    const usernameText = "שם משתמש"
    const passwordText = "סיסמא"
    const emailText = "אימייל"
    const firstNameText = "שם פרטי"
    const lastNameText = "שם משפחה"
    const isHostText = " האם תרצה להיות מארח?"
    const phoneNumberText = "מספר פלאפון"
    const descriptionText = "תיאור עליי"
    const interestsText = "תחומי עניין"
    const uploadImageText = "העלאת תמונת פרופיל"
    const closeText = "סגור"
    const registerText = "הירשם"
    const navigate = useNavigate();
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [description, setDescription] = useState("")
    const [email, setEmail] = useState("")
    const [phoneNumber, setPhoneNumber] = useState("")
    const [isHost, setIsHost] = useState(false)
    const [interests, setInterests] = useState("")
    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [avatarImage, setAvatarImage] = useState("")
    const [openModal, setOpenModal] = useState(false)
    const [enableRegister, setEnableRegister] = useState(false)
    useEffect(() => {
        if (username != "" && password != "" && email != "") {
            setEnableRegister(true)
        } else {
            setEnableRegister(false)
        }
    })
    const handleRegister = event => {
        event.preventDefault();
        event.target.className = "ui loading button";
        RegisterUser().catch(console.error);
    }
    const RegisterUser = async () => {
        const userInfo = {
            username: username,
            password: password,
            description: description,
            email: email,
            phoneNumber: phoneNumber,
            isHost: isHost,
            interests: interests,
            firstName: firstName,
            lastName: lastName,
            avatarImage: avatarImage,
        };
        apiService.RegistrationService.register(userInfo).then(r => {

        });
        navigate('/*')
    }
    const onUploadImage = props =>{
        setAvatarImage(props[1])
    };
    return (
        <>
            <Container text>
                <Form>
                    <Form.Field>
                        <label> {usernameText}</label>
                        <input
                            type="text"
                            onChange={e => setUsername(e.target.value)}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label> {passwordText}</label>
                        <input
                            type="password"
                            onChange={e => setPassword(e.target.value)}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label> {emailText}</label>
                        <input
                            type="email"
                            onChange={e => setEmail(e.target.value)}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label> {firstNameText}</label>
                        <input
                            type="text"
                            onChange={e => setFirstName(e.target.value)}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label> {lastNameText}</label>
                        <input
                            type="text"
                            onChange={e => setLastName(e.target.value)}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>{isHostText}</label>
                        <input
                            type="checkbox"
                            onChange={e => setIsHost(!isHost)}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label> {phoneNumberText}</label>
                        <input
                            type="number"
                            onChange={e => setPhoneNumber(e.target.value)}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>  {descriptionText} </label>
                        <input
                            control={TextArea}
                            onChange={e => setDescription(e.target.value)}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label> {interestsText}</label>
                        <input
                            type="text"
                            onChange={e => setInterests(e.target.value)}
                        />
                    </Form.Field>
                    <Modal
                        onClose={() => setOpenModal(false)}
                        onOpen={() => setOpenModal(true)}
                        open={openModal}
                        trigger={<Button>{uploadImageText}</Button>}
                    >
                        <Modal.Header>Select a Photo</Modal.Header>
                        <Modal.Content>
                            <Modal.Description>
                                <UploadImage onUpload={onUploadImage} />
                            </Modal.Description>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button content={closeText} onClick={() => setOpenModal(false)} />
                        </Modal.Actions>
                    </Modal>
                </Form>
                <br /> <br />
                <Button fluid onClick={handleRegister} disabled={!enableRegister}>
                    {registerText}
                </Button>
            </Container>

        </>
    )
};

export default Register;
