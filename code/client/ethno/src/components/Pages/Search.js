import React, {useEffect, useState} from "react";
import {Button, Container, Dropdown, Header, Input} from "semantic-ui-react";
import apiService from "../../services/api/api";
import {useNavigate} from "react-router-dom";

const times = [
    {key: 1, value: "בוקר", text: "בוקר"},
    {key: 2, value: "צהריים", text: "צהריים"},
    {key: 3, value: "ערב", text: "ערב"}
];


const Search = () => {
    //i18n
    // const food = "אוכל";
    // const art = "אומנות";
    // const music = "מוזיקה";
    // const entertainment = "פנאי ובידור";
    // const trip = "טיולים";
    const attractionKindTitle = "בחירת סוג חוויה";
    const attractionCultureTitle = "תרבות";
    const attractionLocationTitle = "מיקום";
    const priceLimitTitle = "הגבלת מחיר";
    const ticketPrice = "עלות לכרטיס";
    const attractionTimeTitle = "זמן";
    const searchText = "חפש!";

    const [attractionKind, setAttractionKind] = useState("");
    const [attractionCulture, setAttractionCulture] = useState("");
    const [attractionLocation, setAttractionLocation] = useState("");
    const [attractionTime, setAttractionTime] = useState("");
    const [enableSearch, setEnableSearch] = useState(true);
    const [listOfCultures, setListOfCultures] = useState([]);
    const [listOfKinds, setListOfKinds] = useState([]);
    const [listOfLocations, setListOfLocations] = useState([]);
    const [listOfTimes, setListOfTimes] = useState(times);
    const [priceLimit, setPriceLimit] = useState(50)
    const navigate = useNavigate();

    const handleSearchClick = event => {
        event.target.className = "ui loading button";
        search().catch(console.error);
    };

    const search = async () => {
        const data = {
            kind: attractionKind,
            culture: attractionCulture,
            location: attractionLocation,
            time: attractionTime,
            limit: priceLimit
        };
        let response = await apiService.AttractionService.searchAttraction(data);
        if (response === "") {
            response = NaN;
        }

        navigate("/result", {
            state: {attractionsList: response}
        });
    };

    useEffect(() => {
        const fetchData = async () => {
            const response = await apiService.AttractionService.initSearchPage();

            const kind = response.kind.map(c => {
                return {
                    key: c.id,
                    value: c.kind,
                    icon: c.icon,
                    text: c.kind
                };
            });
            const culture = response.culture.map(c => {
                return {
                    key: c.id,
                    value: c.culture,
                    text: c.culture
                };
            });
            const city = response.city.map(c => {
                return {
                    key: c.id,
                    value: c.city,
                    text: c.city
                };
            });
            setListOfCultures(culture);
            setListOfKinds(kind);
            setListOfLocations(city);
        };
        fetchData().catch(console.error);
    }, []);

    const handlePriceLimitInput = e =>{
        let priceLimitInput;
        if (isNaN(e.target.value ) || e.target.value < 50 ){
            priceLimitInput = 50;
        }
        else{
            priceLimitInput = parseInt(e.target.value);
        }
        setPriceLimit(priceLimitInput)
        e.target.value = priceLimitInput;
    }

    const handleChangeDropdownSelection = (e, value, stateToChange) => {
        stateToChange(value);
    };
    useEffect(() => {
        if (
            attractionKind !== "" &&
            attractionCulture !== "" &&
            attractionLocation !== "" &&
            attractionTime !== "" &&
            priceLimit >= 50
        ) {
            setEnableSearch(false);
        } else {
            setEnableSearch(true);
        }
    }, [attractionKind, attractionCulture, attractionLocation, attractionTime, priceLimit]);

    return (
        <>
            <br/>
            <br/>
            <Container text>
                <Header as="h2">{attractionKindTitle}</Header>
                <Dropdown
                    placeholder={attractionKindTitle}
                    fluid
                    selection
                    clearable
                    options={listOfKinds}
                    onChange={
                        (e, {value}) => {
                            handleChangeDropdownSelection(e, value, setAttractionKind);
                        }
                    }
                />
                <br/>
                <br/>
            </Container>
            <Container text>
                <Header as="h2">{attractionCultureTitle}</Header>
                <Dropdown
                    placeholder={attractionCultureTitle}
                    fluid
                    selection
                    clearable
                    options={listOfCultures}
                    onChange={
                        (e, {value}) => {
                            handleChangeDropdownSelection(e, value, setAttractionCulture);
                        }
                    }
                />
            </Container>
            <br/>
            <br/>
            <Container text>
                <Header as="h2">{attractionLocationTitle}</Header>
                <Dropdown
                    placeholder={attractionLocationTitle}
                    fluid
                    selection
                    clearable
                    options={listOfLocations}
                    onChange={
                        (e, {value}) => {
                            handleChangeDropdownSelection(e, value, setAttractionLocation);
                        }
                    }
                />
                <Header as="h2">{attractionTimeTitle}</Header>
                <Dropdown
                    placeholder={attractionTimeTitle}
                    fluid
                    selection
                    options={listOfTimes}
                    clearable
                    onChange={
                        (e, {value}) => {
                            handleChangeDropdownSelection(e, value, setAttractionTime);
                        }
                    }
                />
            </Container>
            <br/>
            <br/>
            <Container text>
                <Header as="h2">{priceLimitTitle}</Header>
                <Input
                    fluid
                    type="number"
                    min="50"
                    max="1000"
                    step="5"
                    placeholder={ticketPrice}
                    onBlur={handlePriceLimitInput}
                />
            </Container>
            <Container text>
                <br/>
                <br/>
                <Button
                    fluid
                    color="teal"
                    onClick={handleSearchClick}
                    disabled={enableSearch}
                >
                    {searchText}
                </Button>
            </Container>
        </>
    );
};

export default Search;
