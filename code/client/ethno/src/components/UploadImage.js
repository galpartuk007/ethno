import React, { useState } from "react";

const UploadImage = (prop) => {
  const [imageSelected, setImageSelected] = useState("");
  const [previewSource, setPreviewSource] = useState("");

  const cloud_name = "dvy04f4fs";

  const handleUploadImage = (e) => {
    setImageSelected(e.target.files[0]);
    previewImage(e.target.files[0]);
  };

  const uploadImage = (e) => {
    e.preventDefault();
    const image_to_upload = imageSelected;
    const data = new FormData();
    data.append("file", image_to_upload);
    data.append("upload_preset", "my_preset");
    data.append("upload_preset", "rplim3ni");

    fetch(`https://api.cloudinary.com/v1_1/${cloud_name}/image/upload`, {
      method: "post",
      body: data,
    })
      .then((resp) => resp.json())
      .then((data) => {
        prop.onUpload([prop.type,data.url])
      })
      .catch((err) => console.log(err));
  };

  const previewImage = (image) => {
    const reader = new FileReader();
    reader.readAsDataURL(image);
    reader.onloadend = () => setPreviewSource(reader.result);
  };

  return (
    <div>
      <div>
        <input
          type="file"
          onChange={handleUploadImage}
        />
        <button onClick={uploadImage} disabled={imageSelected == ""}>
          Upload Image
        </button>
      </div>
      {previewSource && <img  className={"ui small image"} src={previewSource} alt="selected image" />}
    </div>
  );
};

export default UploadImage;
