import React, { useEffect, useState } from "react";
import logo from "./Images/ethno.jpg";
import apiService from "../services/api/api";
import { useEthnoContext } from "../state/EthnoContextProvider";
import { Image } from "semantic-ui-react";
import { Link } from "react-router-dom";

const logout = () => {
  localStorage.removeItem("token");
  window.location.reload(false);
};

const myOrders = "ההזמנות שלי";
const search = "חיפוש";
const createAttraction = "צור חוויה";
const myAttraction = "החוויות שלי";
const logoutText = "יציאה";
const NavBar = () => {
  const { authState } = useEthnoContext();

  const [avatarImage, setAvatarImage] = useState();
  const [isUserHost, setIsUserHost] = useState(true);
  useEffect(async () => {
    await apiService.NavBarService.getUserInfo(authState.username).then(
      response => {
        setAvatarImage(response.avatar_image);
        setIsUserHost(response.is_user_host);
      }
    );
  }, []);

  return (
    <div className="container">
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Image size="tiny" src={logo} alt="logo" className="img-fluid" />
        <Image avatar src={avatarImage} className="img-fluid" />
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <Link style={{ textDecoration: "none" }} to="/search">
              <li className="nav-item active">
                <a className="nav-link">
                  <strong> {search}</strong>
                </a>
              </li>
            </Link>
            <Link
              style={{ textDecoration: "none" }}
              to="/attractionsUserOrdered"
            >
              <li className="nav-item active">
                <a className="nav-link">
                  <strong> {myOrders}</strong>
                </a>
              </li>
            </Link>
            {isUserHost && (
              <Link style={{ textDecoration: "none" }} to="/create">
                <li className="nav-item active">
                  <a className="nav-link" href="#">
                    <strong>{createAttraction} </strong>{" "}
                  </a>
                </li>
              </Link>
            )}
            {isUserHost && (
              <Link style={{ textDecoration: "none" }} to="/myAttractions">
                <li className="nav-item active">
                  <a className="nav-link" href="#">
                    {" "}
                    <strong>{myAttraction} </strong>{" "}
                  </a>
                </li>
              </Link>
            )}
            <li className="nav-item active float-right">
              <a className="nav-link" href="#" onClick={logout}>
                {" "}
                <strong> {logoutText} </strong>
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
};

export default NavBar;
