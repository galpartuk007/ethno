import React, {useEffect, useState} from "react";
import {useLocation, useNavigate} from "react-router-dom";
import apiService from "../../services/api/api";
import {useEthnoContext} from "../../state/EthnoContextProvider";
import {Button, Checkbox, Container, Form, Header, List, TextArea} from "semantic-ui-react";

const Reservation = ({}) => {
    const location = useLocation();

    const [fullName, setFullName] = useState("");
    const [ticketsNumber, setTicketsNumber] = useState(0);
    const [phoneNumber, setPhoneNumber] = useState("");
    const [email, setEmail] = useState("");
    const [message, setMessage] = useState("");
    const [checkBoxChecked, setCheckBoxChecked] = useState(false);
    const [enableReservation, setEnableReservation] = useState(true);
    const {authState} = useEthnoContext();
    const navigate = useNavigate();
    useEffect(() => {
        if (
            fullName !== "" &&
            ticketsNumber > 0 &&
            phoneNumber !== "" &&
            email !== "" &&
            checkBoxChecked !== false
        ) {
            setEnableReservation(false);
        } else {
            setEnableReservation(true);
        }
    });

    const handleSubmit = e => {
        e.preventDefault();
        const data = {
            fullName: fullName,
            ticketsNumber: ticketsNumber,
            phoneNumber: phoneNumber,
            email: email,
            message: message,
            username: authState.username,
            attraction_id: location.state.attractionInfo.id
        };
        apiService.AttractionService.makeReservation(data).then(
            navigate("/")
        );
    };
    return (
        <>
            {/*// Header Section*/}
            <Container text>
                <Header as="h2">בחירת סוג חוויה</Header>
                <Header as="h3">{location.state.attraction.attraction_name}</Header>
                <List bulleted>
                    <List.Item>
                        מיקום:{" "}
                        {location.state.attraction.Address +
                            "," +
                            location.state.attraction.city}
                    </List.Item>
                    <List.Item>ניתן לבטל עד 48 שעות</List.Item>
                    <List.Item>זמן סדנה: {location.state.attraction.duration}</List.Item>
                    <List.Item>
                        מקומות פנויים: {location.state.attractionInfo.FreeSpaces}
                    </List.Item>
                    <List.Item>תאריך: {location.state.attractionInfo.date}</List.Item>
                </List>
            </Container>
            <br/> <br/>
            {/*// Form Section*/}
            <Container text>
                <Form>
                    <Form.Field>
                        <label>שם מלא</label>
                        <input type="text" onChange={e => setFullName(e.target.value)}/>
                    </Form.Field>
                    <Form.Field>
                        <label>מספר כרטיסים</label>
                        <input
                            type="number"
                            max={location.state.attractionInfo.FreeSpaces}
                            min={1}
                            onChange={e => setTicketsNumber(e.target.value)}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>מספר טלפון</label>
                        <input type="text" onChange={e => setPhoneNumber(e.target.value)}/>
                    </Form.Field>
                    <Form.Field>
                        <label>כתובת מייל</label>
                        <input onChange={e => setEmail(e.target.value)}/>
                    </Form.Field>

                    <Form.Field
                        control={TextArea}
                        placeholder="כאן תוכל לרשום פרטים או בקשות אישיות שתרצה לבקש מהאמן"
                        label="הודעה"
                        onChange={e => setMessage(e.target.value)}
                    />
                    <Form.Field>
                        <Checkbox
                            onChange={() => {
                                setCheckBoxChecked(!checkBoxChecked);
                            }}
                            label="קראתי ואני מסכים לתנאי השימוש"
                        />
                    </Form.Field>
                    <Button fluid onClick={handleSubmit} disabled={enableReservation}>
                        שלח
                    </Button>
                </Form>
            </Container>
        </>
    );
};

export default Reservation;
