import React from "react";
import {Route, Routes} from "react-router-dom";
import "./App.css";
import UploadImage from "./components/UploadImage";
import Notifications from "./components/Pages/Notifications";
import Reservation from "./components/Pages/Reservation";
import {LoginPage} from "./components/login/LoginPage";
import {useAuth} from "./hooks/useAuth";
import Search from "./components/Pages/Search";
import SearchResults from "./components/Pages/SearchResults";
import Edit from "./components/Pages/MyAttractions";
import Layout from "./hoc/Layout";
import CreateAttraction from "./components/Pages/CreateAttraction";
import Card from "./components/List/PreviewCard";
import CreateEvent from "./components/Pages/CreateEvent";
import AttractionInfo from "./components/Pages/AttractionInfo";
import AttractionNotFound from "./components/Pages/AttractionNotFound";
import CardList from "./components/List/CardList";
import UserOrder from "./components/Pages/UserOrder";
import Register from "./components/Pages/Register";

const App = () => {
    const {isAuthenticated} = useAuth();

    if (isAuthenticated()) {
        return (
            <Layout>
                <Routes>
                    <Route path="/search" element={<Search/>}/>
                    <Route path="/uploadImage" element={<UploadImage/>}/>
                    <Route path="/notifications" element={<Notifications/>}/>
                    <Route path="/reservation" element={<Reservation/>}/>
                    <Route path="/result" element={<SearchResults/>}/>
                    <Route path="/create" element={<CreateAttraction/>}/>
                    <Route path="/myAttractions" element={<Edit/>}/>
                    <Route path="/cardPreview" element={<Card/>}/>
                    <Route path="/createEvent" element={<CreateEvent/>}/>
                    <Route path="/attractionInfo" element={<AttractionInfo/>}/>
                    <Route path="/attractionNotFound" element={<AttractionNotFound/>}/>
                    <Route path="/cardPreviewList" element={<CardList/>}/>
                    <Route path="*" element={<UserOrder/>}/>
                </Routes>
            </Layout>
        );
    } else {
        return (
            <Routes>
                <Route path="*" element={<LoginPage/>}/>
                <Route path="/register" element={<Register/>}/>
            </Routes>
        );
    }
};

export default App;
