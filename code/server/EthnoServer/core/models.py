from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models


# todo:map which field can be blank and which are required
class User(AbstractUser):
    phone = models.CharField(max_length=10, default=" ")
    isHost = models.BooleanField(default=False)
    description = models.CharField(max_length=100, default="none")
    interests = models.CharField(max_length=100, default="none")
    avatarImage = models.CharField(max_length=200,
                                   default="https://res.cloudinary.com/dvy04f4fs/image/upload/v1649086236/kpmwvsrowkfjtaibgx0k.png")


class Attraction(models.Model):
    attraction_name = models.CharField(max_length=50)
    host = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='operator_name')
    description = models.TextField()
    price = models.DecimalField(max_digits=4, decimal_places=1)
    duration = models.CharField(max_length=50)
    kind = models.CharField(max_length=50, default="music")
    culture = models.CharField(max_length=50, default="yeman")
    part_of_the_day = models.CharField(max_length=50, default="morning")
    Address = models.CharField(max_length=100)
    city = models.CharField(max_length=100, default="ashdod")
    rating = models.FloatField(default=0)
    amount_of_rating = models.IntegerField(default=0)
    summary = models.TextField(default="")
    bannerImage = models.CharField(max_length=200,
                                   default="https://res.cloudinary.com/dvy04f4fs/image/upload/v1649082671/fqf72kqairjupuzh7aty.jpg")


class AvailableAttraction(models.Model):
    AttractionID = models.ForeignKey(Attraction, on_delete=models.CASCADE, related_name='attractionID')
    FreeSpaces = models.IntegerField()
    date = models.CharField(max_length=10)
    startTime = models.TimeField()
    endTime = models.TimeField()
    day = models.CharField(max_length=20)


class UserMessage(models.Model):
    userID = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='userID')
    message = models.CharField(max_length=50)


class AttractionsUserOrdered(models.Model):
    availableAttractionID = models.ForeignKey(AvailableAttraction, on_delete=models.CASCADE,
                                              related_name='availableAttractionID')
    userID = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='ordering_user')
    full_name = models.CharField(max_length=50, default="")
    tickets_number = models.IntegerField(default="")
    phone_number = models.CharField(max_length=10, default="0508329029")
    message = models.TextField(default="")


class AttractionImages(models.Model):
    AttractionID = models.ForeignKey(Attraction, on_delete=models.CASCADE,
                                     related_name='AttractionID')
    image = models.CharField(max_length=200)


class AttractionKind(models.Model):
    icon = models.CharField(max_length=50, default="tag icon")
    kind = models.CharField(max_length=50, default="")

    class Meta:
        unique_together = ('icon', 'kind',)


class AttractionCulture(models.Model):
    culture = models.CharField(max_length=50, default="", unique=True)


class AttractionCities(models.Model):
    city = models.CharField(max_length=50, default="", unique=True)
