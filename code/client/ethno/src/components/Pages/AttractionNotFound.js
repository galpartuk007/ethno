import React from 'react';
import { useNavigate } from "react-router-dom"
import {Button, Container, Header} from "semantic-ui-react";
import apiService from "../../services/api/api";

const AttractionNotFound = () =>{
    const sorryText = "סליחה"
    const apologyText = "לא הצלחנו למצוא את מה שחיפשת אבל יש לנו עוד הרבה מה להציע!"
    const searchText = "חפש שוב"
    const randomSearch ="מצא משהו"

    const navigate =useNavigate();

    function handleSearchClick() {
        navigate("/search");
    }

    const handleRandomSearchClick = async () =>{
        let response = await  apiService.AttractionService.getRandomAttraction()
        navigate("/attractionInfo", {
            state: {attraction: response}
        })
    }

    return(
        <Container text textAlign={'center'}>
            <Header as="h1">{sorryText} :(</Header>
            <Header as="h3">{apologyText}</Header>
            <Button className="ui black basic button" floated='right' onClick={handleSearchClick} >{searchText}!
            </Button>
            <Button className="ui teal basic button" floated="left" onClick={handleRandomSearchClick} >{randomSearch}!
            </Button>
        </Container>
    )
}

export default AttractionNotFound;