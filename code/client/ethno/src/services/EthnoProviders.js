import React from "react";
import { HashRouter as Router } from "react-router-dom";
import EthnoContextProvider from "../state/EthnoContextProvider";
//import I18NProvider from "../i18n/provider";

export const EthnoProviders = ({ children }) => {
  return (
    <EthnoContextProvider>
      <Router>
        {/*
            <I18NProvider>{children}</I18NProvider>
          */}
        {children}
      </Router>
    </EthnoContextProvider>
  );
};
