import React from "react";
import AvailableAttractionInfo from "./AvailableAttractionInfo";

const AvailableAttractionList = ({ attractionList }) => {
  const defaultmassage = "אין אירועים קרובים";
  let listOfAttraction = [];
  if (attractionList != undefined) {
    listOfAttraction = attractionList.map((availableAttractionInfo) => {
      return (
        <AvailableAttractionInfo
          key={availableAttractionInfo.id}
          availableAttractionInfo={availableAttractionInfo}
        />
      );
    });
  } else {
    listOfAttraction = <div>{defaultmassage}</div>;
  }
  return <div className="ui relaxed divided list">{listOfAttraction}</div>;
};

export default AvailableAttractionList;
