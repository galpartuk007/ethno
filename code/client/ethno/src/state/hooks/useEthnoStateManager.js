import { useCallback, useState } from "react";

const ethnoStateInitialState = {};

export const useEthnoStateStateManager = () => {
  const [ethnoState, setEthnoState] = useState(ethnoStateInitialState);

  const updateEthnoState = useCallback((updatedEthnoState) => {
    setEthnoState((currentEthnoState) => ({
      ...currentEthnoState,
      ...updatedEthnoState,
    }));
  }, []);

  return { ethnoState, updateEthnoState: updateEthnoState };
};
