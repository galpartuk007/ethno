import React from "react";
import {Button, Card, Image} from "semantic-ui-react";
import {useNavigate} from "react-router-dom";

const PreviewCard = props => {
    const navigate = useNavigate();
    const attraction = props.data;

    //i18n
    const show = "לצפייה בחוויה";
    const ticketPrice = "מחיר לכרטיס";
    const newShekel = 'ש"ח';

    const handleClickEditButton = () => {
        navigate("/attractionInfo", {
            state: {attraction: attraction}
        });
    };

    return (
        <>
            <Card fluid color="teal">
                <Card.Content>
                    <Card.Header>
                        {attraction ? attraction.attraction_name : ""}{" "}
                    </Card.Header>
                    <Card.Meta>
                        {attraction ? attraction.Address + ", " + attraction.city : ""}
                    </Card.Meta>
                    <Card.Description>
                        <Image
                            src={attraction ? attraction.bannerImage : ""}
                            floated="right"
                            size="small"
                        />
                        <strong>{attraction ? attraction.description : ""}</strong>
                        <br/>
                        <strong>{attraction ? attraction.summary : ""}</strong>
                        <br/>
                        <strong>{ticketPrice} : {attraction ? attraction.price : ""} {newShekel}</strong>
                    </Card.Description>
                </Card.Content>
                <Button basic onClick={handleClickEditButton}>
                    {show}
                </Button>
            </Card>
        </>
    );
};

export default PreviewCard;
