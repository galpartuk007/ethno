from django.contrib import admin

from .models import User, Attraction, AvailableAttraction, AttractionsUserOrdered, UserMessage,AttractionImages, AttractionCulture, AttractionKind

admin.site.register(User)
admin.site.register(Attraction)
admin.site.register(AvailableAttraction)
admin.site.register(AttractionsUserOrdered)
admin.site.register(UserMessage)
admin.site.register(AttractionImages)
admin.site.register(AttractionCulture)
admin.site.register(AttractionKind)
