import React, { useEffect, useState } from "react";
import apiService from "../../services/api/api";
import { useEthnoContext } from "../../state/EthnoContextProvider";
import { useNavigate } from "react-router-dom";

const MyAttractions = () => {
  const [AttractionsList, setAttractionsList] = useState("");
  const { authState } = useEthnoContext();
  const navigate = useNavigate();

  useEffect(() => {
    const fetchAttractions = async () => {
      apiService.AttractionService.getAllUserAttractions(
        authState.username
      ).then(response => {
        setAttractionsList(response);
        navigate("/result", {
          state: { attractionsList: response }
        });
      });
    };

    // call the function
    fetchAttractions().catch(console.error);
  }, []);

  return <div>my attraction</div>;
};

export default MyAttractions;
