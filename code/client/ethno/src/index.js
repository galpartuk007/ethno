import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import {EthnoProviders} from "./services/EthnoProviders";

ReactDOM.render(
    <EthnoProviders>
        <App/>
    </EthnoProviders>,
    document.getElementById("root")
);
