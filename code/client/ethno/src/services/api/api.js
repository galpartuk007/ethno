import Axios from "axios";

const $axios = Axios.create({
    baseURL: "/api/",
    headers: {"Content-Type": "application/json"}
});

class AuthService {
    static login(username, password) {
        return $axios
            .post(`auth/obtain-token/`, {username, password})
            .then(response => response.data);
    }
}

class RegistrationService {
    static register(data) {
        return $axios
            .post("register/", {
                params: {
                    username: data.username,
                    password: data.password,
                    description: data.description,
                    email: data.email,
                    phone_number: data.phoneNumber,
                    isHost: data.isHost,
                    interests: data.interests,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    avatarImage: data.avatarImage,
                }
            })
            .then(response => response.data);

    }
}

class NavBarService {
    static getUserInfo(term) {
        return $axios
            .get("get_user_info", {
                params: {username: term}
            })
            .then(response => response.data);
    }
}

class AttractionService {
    static getAttractions(term) {
        return $axios
            .get("get_attraction/", {
                params: {attraction_id: term}
            })
            .then(response => response.data);
    }

    static searchAttraction(term) {
        return $axios
            .get("search_attraction/", {
                params: {
                    kind: term.kind,
                    culture: term.culture,
                    time: term.time,
                    location: term.location,
                    priceLimit: term.limit
                }
            })
            .then(response => response.data);
    }

    static getAllUserAttractions(userName) {
        return $axios
            .get("get_all_user_attractions/", {
                params: {
                    username: userName
                }
            })
            .then(response => response.data);
    }

    static createNewAttraction(data) {
        return $axios
            .post("create_new_attraction/", {
                params: {
                    attractionName: data.name,
                    description: data.description,
                    city: data.city,
                    address: data.address,
                    duration: data.duration,
                    price: data.price,
                    kind: data.kind,
                    culture: data.culture,
                    partOfDay: data.partOfDay,
                    summary: data.summary,
                    bannerImage: data.bannerImage,
                    username: data.userName,
                    images: data.images
                }
            })
            .then(response => response.data);
    }

    static createNewEvent(data) {
        return $axios
            .post("create_new_event/", {
                params: {
                    attractionId: data.attractionId,
                    date: data.date,
                    day: data.day,
                    startTime: data.startTime,
                    endTime: data.endTime,
                    numberOfTickets: data.numberOfTickets
                }
            })
            .then(response => response.data);
    }

    static getAllUserReservations(userName) {
        return $axios
            .get("get_all_user_reservations", {
                params: {
                    userName: userName
                }
            })
            .then(response => response.data);
    }

    static makeReservation(data) {
        return $axios

            .post("make_reservation/", {
                params: {
                    fullName: data.fullName,
                    ticketsNumber: data.ticketsNumber,
                    phoneNumber: data.phoneNumber,
                    email: data.email,
                    message: data.message,
                    username: data.username,
                    attraction_id: data.attraction_id
                }
            })
            .then(response => response.data);
    }

    static initSearchPage() {
        return $axios.get("init_search_page/", {}).then(response => response.data);
    }

    static getRandomAttraction() {
        return $axios.get("get_random_attraction/", {}).then(response => response.data);
    }
}

const apiService = {AttractionService, AuthService, NavBarService, RegistrationService};

export default apiService;
