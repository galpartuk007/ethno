import React from "react";

export const PageHeader = ({ title, description }) => {
  return (
    <div>
      <h2>{title}</h2>
      <h3>{description}</h3>
    </div>
  );
};
