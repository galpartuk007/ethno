import apiService from "../services/api/api";
import {useEthnoContext} from "../state/EthnoContextProvider";

export const useAuth = () => {
    const {updateAuthState, authState} = useEthnoContext();
    const login = async (username, password) => {
        try {
            const token = await apiService.AuthService.login(username, password);
            if (token) {
                localStorage.setItem("token", token.token);
                localStorage.setItem("username", username);
                updateAuthState({token: token.token, username: username});
            }

            return token.token;
        } catch (err) {
            return null;
        }
    };

    const isAuthenticated = () => {
        return authState.token !== "";
    };
    return {login, isAuthenticated};
};
