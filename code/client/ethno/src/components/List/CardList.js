import React from "react";
import PreviewCard from "./PreviewCard";
import OrderCard from "./OrderCard";

const CardList = ({listOfAttractions, listType}) => {
    let cards = [];
    const attractionCardsList = listOfAttractions;

    switch (listType) {
        case "attractionInfoFromSearch":
            cards = attractionCardsList.map(attractionInfo => {
                return <PreviewCard key={attractionInfo.attraction_ID} data={attractionInfo}/>;
            });
            break;

        case "attractionInfoFromUserOrder":
            cards = attractionCardsList.map(attractionInfo => {
                return <OrderCard key={attractionInfo.id} data={attractionInfo}/>;
            });
            break;
    }

    return <div className="ui relaxed divided list">{cards}</div>;
};

export default CardList;
