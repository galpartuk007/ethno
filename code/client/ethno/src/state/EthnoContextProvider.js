import React, {createContext, useContext} from "react";
import {useUserInfoContext} from "./hooks/useUserInfoContext";

import {useAuthContext} from "./hooks/useAuthContext";

const ethnoContext = createContext({});

const EthnoContextProvider = ({children}) => {
    const userInfoState = useUserInfoContext();
    const authState = useAuthContext();

    const ethnoStateModel = {
        ...userInfoState,
        ...authState,
    };
    return (
        <ethnoContext.Provider value={ethnoStateModel}>
            {children}
        </ethnoContext.Provider>
    );
};

export default EthnoContextProvider;

export const useEthnoContext = () => useContext(ethnoContext);
