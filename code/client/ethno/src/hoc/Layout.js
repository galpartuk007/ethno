import React from "react";
import NavBar from "../components/NavBar";

const Layout = (props) => {
    return (
        <div dir="rtl">
            <NavBar/>
            {props.children}
        </div>
    );
};

export default Layout;
