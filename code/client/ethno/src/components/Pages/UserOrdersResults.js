import React, {useEffect, useState} from "react";
import CardList from "../List/CardList";
import AttractionNotFound from "./AttractionNotFound";

const UserOrdersResults = ({listOfAttractions}) => {
    const listType = "attractionInfoFromUserOrder";

    if (listOfAttractions !== undefined && listOfAttractions.length > 0) {
        return <CardList listOfAttractions={listOfAttractions} listType={listType} />;
    } else {
        return <AttractionNotFound />;
    }

};
export default UserOrdersResults