import React, {useState} from "react";
import {useAuth} from "../../hooks/useAuth";
import {Button, Container, Form, Header, Image, Label} from "semantic-ui-react";
import logo from "../Images/ethno.jpg";
import {Link} from "react-router-dom";

export const LoginPage = () => {
    const welcome = "ברוכים הבאים לאתנו"
    const containerSecondTitle = "אפליקציה שחושפת בפניכם תרבויות שלא הכרתם, ממש מתחת לאף!";
    const loginText = "התחברות"
    const usernameText = "שם משתמש";
    const passwordText = "סיסמא";
    const signupText = "הירשם לאתנו";
    const {login} = useAuth();
    const [credentials, setCredentials] = useState({
        username: "",
        password: "",
        showPassword: false,
    });

    const handleChange = (prop) => (event) =>
        setCredentials({...credentials, [prop]: event.target.value});

    const onSubmit = async () => {
        try {
            login(credentials.username, credentials.password);
        } catch (error) {
            alert("wrong credentials");
            setCredentials({
                username: "",
                password: "",
                showPassword: false,
            });
        }
    };

    return (
        <div dir="rtl">
            <Container text textAlign='center'>
                <Image src={logo} centered/>
                <Header as="h2">{welcome}</Header>
                <p><strong>{containerSecondTitle}</strong></p>
            </Container>
            <Container text>
                <Form>
                    <Form.Field>
                        <label>{loginText}</label>
                        <input type="text"
                               value={credentials.username}
                               onChange={handleChange("username")}
                               placeholder={usernameText}
                        />
                        <br/> <br/>
                        <input type="password"
                               value={credentials.password}
                               onChange={handleChange("password")}
                               placeholder={passwordText}
                        />
                    </Form.Field>
                </Form>
                <br/>
                <Container textAlign='center'>
                    <Link to="/search">
                        <Button
                            onClick={onSubmit}
                            disabled={credentials.password === "" || credentials.username === ""}
                            color="teal"
                        >
                            {loginText}
                        </Button>
                    </Link>
                    <Link to="/register">
                    <div style={{color: "teal"}}><u><strong>{signupText}</strong> </u></div>
                    </Link>
                </Container>
            </Container>
        </div>
    );
};
