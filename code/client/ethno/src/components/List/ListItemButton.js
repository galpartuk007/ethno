import React from "react";
import { List } from "semantic-ui-react";

const ListItemButton = (props) => {
    const culture = props.attractionCulture;
    return (
        <List.Item onClick={props.onClick}>
          <List.Content>
            <i className="ui grey basic button" >{culture}</i>
          </List.Content>
        </List.Item>);
};

export default ListItemButton;