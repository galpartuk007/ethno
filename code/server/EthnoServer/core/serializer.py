from rest_framework import serializers

from .models import User, Attraction, AttractionsUserOrdered, AvailableAttraction, UserMessage, AttractionImages, \
    AttractionCulture, AttractionKind, AttractionCities


class UserMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserMessage
        fields = "__all__"


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = "__all__"


class AttractionSerializer(serializers.ModelSerializer):
    operator_name = UserSerializer(read_only=True)

    class Meta:
        model = Attraction
        fields = "__all__"


class AvailableAttractionSerializer(serializers.ModelSerializer):
    AttractionID = AttractionSerializer(read_only=True)

    class Meta:
        model = AvailableAttraction
        fields = "__all__"


class AttractionsUserOrderedSerializer(serializers.ModelSerializer):
    availableAttractionID = AvailableAttractionSerializer(read_only=True)
    ordering_user = UserSerializer(read_only=True)

    class Meta:
        model = AttractionsUserOrdered
        fields = "__all__"


class AttractionImagesSerializer(serializers.ModelSerializer):
    Attraction = AttractionSerializer(read_only=True)

    class Meta:
        model = AttractionImages
        fields = "__all__"


class AttractionCultureSerializer(serializers.ModelSerializer):
    class Meta:
        model = AttractionCulture
        fields = '__all__'


class AttractionKindSerializer(serializers.ModelSerializer):
    class Meta:
        model = AttractionKind
        fields = "__all__"


class AttractionCitiesSerializer(serializers.ModelSerializer):
    class Meta:
        model = AttractionCities
        fields = "__all__"
