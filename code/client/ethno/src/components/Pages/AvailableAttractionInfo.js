import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import AttractionContext from "../../state/AttractionContext";

const AvailableAttractionInfo = ({ availableAttractionInfo }) => {
  //i18n
  const firstEventDay = "יום ";
  const endEventFreeTickets = " כרטיסים";
  const firstEventFreeTickets = "נותרו ";
  const buttonTextOrderAttraction = "הזמן אירוע";

  const navigate = useNavigate();
  const attraction = useContext(AttractionContext);

  const ReserveAttraction = event => {
    event.preventDefault();
    navigate("/reservation", {
      state: {
        attraction: attraction,
        attractionInfo: availableAttractionInfo
      }
    });
  };

  const canOrder = () => {
    return availableAttractionInfo.FreeSpaces > 0;
  };

  return (
    <div className="ui segment">
      <div className="content" style={{ float: "right" }}>
        <div className="content" style={{ paddingBottom: "5px" }}>
          {firstEventDay} {availableAttractionInfo.day} -{" "}
          {availableAttractionInfo.date}
        </div>
        <div className="content">
          {availableAttractionInfo.startTime} -{" "}
          {availableAttractionInfo.endTime}
        </div>
      </div>
      <div className="content" style={{ float: "left" }}>
        {canOrder() && (
          <button className="ui teal basic button" onClick={ReserveAttraction}>
            <b>{buttonTextOrderAttraction}</b>
          </button>
        )}
        <br />
        <p style={{paddingRight:"7%"}}>{firstEventFreeTickets}
        {availableAttractionInfo.FreeSpaces}
        {endEventFreeTickets}</p>
      </div>
      <br />
      <br />
    </div>
  );
};

export default AvailableAttractionInfo;
