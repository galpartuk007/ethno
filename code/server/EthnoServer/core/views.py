import random

from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

from .models import Attraction, AvailableAttraction, AttractionImages, AttractionsUserOrdered, User, AttractionCulture, \
    AttractionKind, AttractionCities
from .serializer import AttractionKindSerializer, AttractionCultureSerializer, AttractionCitiesSerializer, \
    AttractionsUserOrderedSerializer


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_attraction(request):
    attraction_id = int(request.GET.get("attraction_id", ""))
    content = get_attraction_by_id(attraction_id)
    return Response(content, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def search(request):
    attractionPriceLimit = int(request.GET.get("priceLimit", ""))
    attractionKind = request.GET.get("kind", "")
    attractionCulture = request.GET.get("culture", "")
    attractionLocation = request.GET.get("location", "")
    attractionTime = request.GET.get("time", "")
    print(attractionKind + " " + attractionCulture + " " + attractionTime + " " + attractionLocation)

    try:
        attractions = Attraction.objects.filter(kind=attractionKind, culture=attractionCulture, city=attractionLocation,
                                                part_of_the_day=attractionTime,
                                                price__range=(0, attractionPriceLimit)).values()
    except Attraction.DoesNotExist:
        attractions = None
    print(attractions)
    if attractions is not None:
        return Response(attractions, status=status.HTTP_200_OK)
    else:
        return Response("", status=status.HTTP_200_OK)


@api_view(['POST'])
@renderer_classes([JSONRenderer])
def create_new_attraction(request):
    print(request.data)
    info = request.data['params']
    user = User.objects.filter(username=info['username'])[0]
    attraction = Attraction(
        attraction_name=info['attractionName'],
        description=info['description'],
        price=info['price'],
        duration=info['duration'],
        kind=info['kind'],
        culture=info['culture'],
        part_of_the_day=info['partOfDay'],
        Address=info['address'],
        city=info['city'],
        rating=0,
        amount_of_rating=0,
        summary=info['summary'],
        bannerImage=info['bannerImage'],
        host_id=user.id
    )
    attraction.save()
    attraction_id = Attraction.objects.get(attraction_name=info['attractionName']).id
    for imageURL in info['images']:
        current_image = AttractionImages(
            image=imageURL,
            AttractionID_id=attraction_id,
        )
        current_image.save()

    return Response("", status=status.HTTP_200_OK)


@api_view(['POST'])
@renderer_classes([JSONRenderer])
def create_new_event(request):
    info = request.data['params']

    event = AvailableAttraction(
        AttractionID_id=info['attractionId'],
        day=info['day'],
        date=info['date'],
        startTime=info['startTime'],
        endTime=info['endTime'],
        FreeSpaces=info['numberOfTickets']
    )
    event.save()
    return Response("", status=status.HTTP_200_OK)


@api_view(['POST'])
@renderer_classes([JSONRenderer])
def make_reservation(request):
    info = request.data['params']
    print(info)

    reservation = AttractionsUserOrdered(
        availableAttractionID=AvailableAttraction.objects.get(id=info['attraction_id']),
        userID=User.objects.filter(username=info['username'])[0],
        full_name=info['fullName'],
        tickets_number=info['ticketsNumber'], phone_number=info['phoneNumber'],
        message=info['message'])
    reservation.save()
    # print(reservation)
    event = AvailableAttraction.objects.get(id=info['attraction_id'])
    event.FreeSpaces = event.FreeSpaces - int(info['ticketsNumber'])
    event.save()
    total_price = reservation.availableAttractionID.AttractionID.price * int(reservation.tickets_number)
    host_message = Mail(
        from_email='ethnoinc1@gmail.com',
        to_emails=reservation.availableAttractionID.AttractionID.host.email,
        subject='בוצעה הזמנה חדשה לאטרקציה ' + reservation.availableAttractionID.AttractionID.attraction_name,
        html_content='שלום, ' + reservation.availableAttractionID.AttractionID.host.first_name + " <br>"
                     + " בוצעה הזמנה לאטרקציה " + reservation.availableAttractionID.AttractionID.attraction_name + " אשר מתקיימת בתאריך " + reservation.availableAttractionID.date
                     + " בשעה " + str(
            reservation.availableAttractionID.startTime) + " בכתובת " + reservation.availableAttractionID.AttractionID.Address + " ," + reservation.availableAttractionID.AttractionID.city + ".<br>"
                     + " שם המזמין " + reservation.full_name + " " + " מספר פלאפון " + reservation.phone_number + " כתובת מייל " +
                     info['email'] + " מספר כרטיסים " + reservation.tickets_number + ". <br>"
                     + " המשתמש צירף את ההודעה הבאה: " + reservation.message + ". <br>" +
                     " לידיעתך, לאטרקציה זו נשארו " + str(
            reservation.availableAttractionID.FreeSpaces) + " מקומות פנויים. ",

    )
    try:
        sg = SendGridAPIClient('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')
        response = sg.send(host_message)
        print(response.status_code)
        print(response.body)
        print(response.headers)
    except Exception as e:
        print(e)

    client_message = Mail(
        from_email='ethnoinc1@gmail.com',
        to_emails=reservation.userID.email,
        subject='ההזמנה לאטרקציה ' + reservation.availableAttractionID.AttractionID.attraction_name + " בוצעה בהצלחה!",
        html_content='שלום, ' + reservation.userID.first_name + " " + reservation.userID.last_name + ". <br>"
                     + "בוצעה בהצלחה הזמנה לאטרקציה " + reservation.availableAttractionID.AttractionID.attraction_name + " על שם " + reservation.full_name + " אשר מתקיימת בתאריך " + reservation.availableAttractionID.date
                     + " בשעה " + str(
            reservation.availableAttractionID.startTime) + " בכתובת " + reservation.availableAttractionID.AttractionID.Address + " ," + reservation.availableAttractionID.AttractionID.city + ".<br>"
                     + " כמות הכרטיסים שהוזמנו: " + str(reservation.tickets_number) + ".<br>"
                     + " מחיר לכרטיס: " + str(reservation.availableAttractionID.AttractionID.price) + ".<br>"
                     + "סהכ: " + str(total_price) + ".<br>"
                     + "נתראה שם!",

    )
    try:
        sg = SendGridAPIClient('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')
        response = sg.send(client_message)
        print(response.status_code)
        print(response.body)
        print(response.headers)
    except Exception as e:
        print(e)
    return Response("", status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_user_info(request):
    username = request.GET.get("username", "")
    user_info = User.objects.filter(username=username)[0]
    content = {
        'avatar_image': user_info.avatarImage,
        'is_user_host': user_info.isHost,
    }
    return Response(content, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def init_search_page(request):
    update_attraction_culture_table()
    update_attraction_kind_table()
    update_attraction_location_table()
    all_attraction_kinds = AttractionKind.objects.all()
    all_attraction_culture = AttractionCulture.objects.all()
    all_attraction_cities = AttractionCities.objects.all()
    serialized_cultures = AttractionCultureSerializer(all_attraction_culture, many=True).data
    serialized_kind = AttractionKindSerializer(all_attraction_kinds, many=True).data
    serialized_cities = AttractionCitiesSerializer(all_attraction_cities, many=True).data
    content = {
        'culture': serialized_cultures,
        'kind': serialized_kind,
        'city': serialized_cities,
    }
    return Response(content, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_all_user_attractions(request):
    user_name = request.GET.get("username", "")
    user_id = User.objects.filter(username=user_name)[0].id
    all_user_attractions = Attraction.objects.filter(host_id=user_id).values()
    return Response(all_user_attractions, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_random_attraction(request):
    all_attraction = list(Attraction.objects.all())
    random_id = random.choice(all_attraction).id
    content = get_attraction_by_id(random_id)
    return Response(content, status=status.HTTP_200_OK)


@api_view(['POST'])
@renderer_classes([JSONRenderer])
def register(request):
    info = request.data['params']
    if info['avatarImage'] == "":
        info['avatarImage'] = 'https://res.cloudinary.com/dvy04f4fs/image/upload/v1649086236/kpmwvsrowkfjtaibgx0k.png'
    User.objects.create_user(username=info['username'], password=info['password'], email=info['email'],
                             phone=info['phone_number'], isHost=info['isHost'], description=info['description'],
                             interests=info['interests'], first_name=info['firstName'],
                             last_name=info['lastName'], avatarImage=info['avatarImage'])

    return Response("", status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_all_user_reservations(request):
    user_name = request.GET.get("userName", "")
    user_id = User.objects.filter(username=user_name)[0].id
    order = AttractionsUserOrdered.objects.filter(userID_id=user_id)
    print(order)
    ser_order = AttractionsUserOrderedSerializer(order, many=True).data
    print(ser_order)
    return Response(ser_order, status=status.HTTP_200_OK)


def update_attraction_culture_table():
    all_attractions = Attraction.objects.all()
    for attraction in all_attractions:
        try:
            AttractionCulture.objects.get(culture=attraction.culture)
        except AttractionCulture.DoesNotExist:
            AttractionCulture(culture=attraction.culture).save()


def update_attraction_kind_table():
    all_attractions = Attraction.objects.all()
    for attraction in all_attractions:
        try:
            AttractionKind.objects.get(kind=attraction.kind)
        except AttractionKind.DoesNotExist:
            AttractionKind(kind=attraction.kind, icon="help").save()


def update_attraction_location_table():
    all_attractions = Attraction.objects.all()
    for attraction in all_attractions:
        try:
            AttractionCities.objects.get(city=attraction.city)
        except AttractionCities.DoesNotExist:
            AttractionCities(city=attraction.city).save()


def get_attraction_by_id(attraction_id):
    attraction = Attraction.objects.get(pk=attraction_id)
    host = attraction.host
    available_attractions = AvailableAttraction.objects.filter(AttractionID=attraction.id).values()
    images = AttractionImages.objects.filter(AttractionID=attraction.id).values()
    content = {
        'attraction_ID': attraction_id,
        'attraction_name': attraction.attraction_name,
        'host_name': host.first_name + ' ' + host.last_name,
        'bannerImage': attraction.bannerImage,
        'rating': attraction.rating,
        'amount_of_rating': attraction.amount_of_rating,
        'description': attraction.description,
        'city': attraction.city,
        'Address': attraction.Address,
        'duration': attraction.duration,
        'price': attraction.price,
        'available_attraction': available_attractions,
        'summary': attraction.summary,
        'attraction_banner': attraction.bannerImage,
        'images': images,
        'user_name': host.username
    }
    return content
