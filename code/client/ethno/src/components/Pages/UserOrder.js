import React, {useEffect, useState} from "react";
import apiService from "../../services/api/api";
import UserOrdersResults from "./UserOrdersResults";
import {useEthnoContext} from "../../state/EthnoContextProvider";

const UserOrder = () => {
    let listOfAttractions = undefined;
    const [cardReservationsList, setCardReservationsList] = useState(undefined);

    const { authState } = useEthnoContext();
    useEffect(() => {
        const fetchData = async () => {
            listOfAttractions = await apiService.AttractionService.getAllUserReservations(authState.username);
            setCardReservationsList( <UserOrdersResults listOfAttractions={listOfAttractions} />);
        };

        fetchData().catch(console.error);
    }, []);

    return <>{cardReservationsList}</>
};

export default UserOrder;
