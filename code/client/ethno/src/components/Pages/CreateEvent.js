import React, { useEffect, useState } from "react";
import { Button, Container, Dropdown, Form, Header } from "semantic-ui-react";
import TimeField from "react-simple-timefield";
import apiService from "../../services/api/api";
import {useNavigate} from "react-router-dom";

const CreateEvent = ({ attractionId, attractionName }) => {
  //i18n
  const dayDesc = "יום";
  const dateDesc = "תאריך";
  const startTimeDesc = "שעת התחלה";
  const endTimeDesc = "שעת סיום";
  const numberOfTicketDesc = "מספר מקומות לחוויה";
  const sunday = "ראשון";
  const monday = "שני";
  const tuesday = "שלישי";
  const wednesday = "רביעי";
  const thursday = "חמישי";
  const friday = "שישי";
  const saturday = "שבת";
  const send = "שלח";
  const navigate = useNavigate();
  const daysOfTheWeekList = [
    {
      key: 1,
      text: sunday,
      value: 1
    },
    {
      key: 2,
      text: monday,
      value: 2
    },
    {
      key: 3,
      text: tuesday,
      value: 3
    },
    {
      key: 4,
      text: wednesday,
      value: 4
    },
    {
      key: 5,
      text: thursday,
      value: 5
    },
    {
      key: 6,
      text: friday,
      value: 6
    },
    {
      key: 7,
      text: saturday,
      value: 7
    }
  ];

  const [day, setDay] = useState("");
  const [date, setDate] = useState("");
  const [startTime, setStartTime] = useState("23:59");
  const [endTime, setEndTime] = useState("23:59");
  const [numberOfTickets, setNumberOfTickets] = useState(0);
  const [enableCreation, setEnableCreation] = useState(true);

  const handleCreateClick = event => {
    event.preventDefault();
    event.target.className = "ui loading button";
    createEvent();
    navigate("/myAttractions");
  };

  const dateFormat = dateInWrongFormat => {
    const arrayOfDate = dateInWrongFormat.split("-");
    setDate(arrayOfDate[2] + "/" + arrayOfDate[1] + "/" + arrayOfDate[0]);
  };

  useEffect(() => {
    if (
      day !== "" &&
      date !== "" &&
      startTime !== "23:59" &&
      endTime !== "23:59" &&
      numberOfTickets !== 0
    ) {
      setEnableCreation(false);
    }
  }, [day, date, startTime, endTime, numberOfTickets]);

  const createEvent = () => {
    const eventData = {
      attractionId: attractionId,
      date: date,
      day: day,
      startTime: startTime,
      endTime: endTime,
      numberOfTickets: numberOfTickets
    };
    apiService.AttractionService.createNewEvent(eventData).catch(console.error);
  };

  return (
    //  TODO: rtl in create event form.
    <div dir="rtl">
      <Container text>
        <Header as="h2">{attractionName}</Header>
        <Form>
          <Form.Field>
            <label>{dayDesc}</label>
            <Dropdown
              clearable
              fluid
              selection
              options={daysOfTheWeekList}
              onChange={e => setDay(e.target.textContent)}
            />
          </Form.Field>
          <Form.Field>
            <label> {dateDesc}</label>
            <input
              type="date"
              required
              placeholder="dd-mm-yyyy"
              min="1997-01-01"
              max="2030-12-31"
              onChange={e => dateFormat(e.target.value)}
            />
          </Form.Field>
          <Form.Field>
            <label>{startTimeDesc}</label>
            <TimeField
              value={startTime}
              onChange={e => setStartTime(e.target.value)}
              input=<input type="text" />
            />
          </Form.Field>
          <Form.Field>
            <label>{endTimeDesc}</label>
            <TimeField
              value={endTime}
              onChange={e => setEndTime(e.target.value)}
              input=<input type="text" />
            />
          </Form.Field>
          <Form.Field>
            <label> {numberOfTicketDesc}</label>
            <input
              type="number"
              min="5"
              max="300"
              step="5"
              placeholder={numberOfTicketDesc}
              onChange={e => setNumberOfTickets(parseInt(e.target.value))}
            />
          </Form.Field>
          <Button fluid onClick={handleCreateClick} disabled={enableCreation}>
            {send}
          </Button>
        </Form>
      </Container>
    </div>
  );
};

export default CreateEvent;
