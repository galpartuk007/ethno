# Ethno
Senior year graduation project.

### System users:
username: Host1
username: Host2
username: Host3
username: Customer

passwords for all: 123456

### Setup env
* Client
    * Run `npm install` from `code/client/ethno`
* Server
    * Install 3rd parties: `pip install -r requirement.txt` from `code/server/EthnoServer`
    * Run DB script from `build/db`
    * Run migration: `python manage.py migrate` from `code/server/EthnoServer`
    * Populate DB: `python .\manage.py loaddata appData` from `code/server/EthnoServer`
    