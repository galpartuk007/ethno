import React, { useState } from "react";
import { useLocation } from "react-router-dom";
import {
  Button,
  Container,
  Grid,
  Header,
  Icon,
  Image,
  Item,
  Modal
} from "semantic-ui-react";
import AttractionContext from "../../state/AttractionContext";
import AvailableAttractionList from "./AvailableAttractionList";
import CreateEvent from "./CreateEvent";
import { useEthnoContext } from "../../state/EthnoContextProvider";

const AttractionInfo = () => {
  // i18n
  const locationText = "מיקום";
  const attractionDuration = " משך סדנה ";
  const attractionCost = "  כרטיס ליחיד ";
  const newShekel = 'ש"ח';
  const secondTitle = " מה מחכה לנו ";
  const thirdTitle = "אירועים קיימים";
  const createEventText = "יצירת אירוע";
  const close = "סגור";
  const location = useLocation();
  const { authState } = useEthnoContext();
  const attraction = location.state.attraction;
  const [openCreateEventModal, setOpenCreateEventModal] = useState(false);

  const isUserIsHost = () => {
    const username = authState.username;
    return (
      username.localeCompare(attraction.user_name, undefined, {
        sensitivity: "base"
      }) === 0
    );
  };

  return (
    <AttractionContext.Provider value={attraction}>
      <Grid centered>
        <Grid.Column width="16">
          <Image
            src={attraction.bannerImage ? attraction.bannerImage : ""}
            size="huge"
            centered
            style={{ marginBottom: "20px" }}
          />
        </Grid.Column>
      </Grid>
      <Container text>
        <Header as="h2"> {attraction.attraction_name}</Header>
        <div style={{ float: "right" }}>
          <Header as="h2">
            <label>
              <Icon name="star" color="teal" />
              {attraction.rating}/5
              <span style={{ color: "grey" }}>
                ({attraction.amount_of_rating})
              </span>
            </label>
          </Header>
        </div>
        <Item.Group>
          <Item>
            <Item.Content>
              <Item.Description>
                <strong>
                  <p>{attraction.description}</p>
                </strong>
              </Item.Description>
            </Item.Content>
          </Item>
        </Item.Group>
        <label>
          <Icon name="map marker alternate" color="teal" />
          {locationText}: {attraction.Address + "," + attraction.city}
        </label>
        <br /> <br />
        <label>
          <Icon name="clock outline" color="teal" />
          {attractionDuration}: {attraction.duration}
        </label>
        <br /> <br />
        <label>
          <Icon name="dollar sign" color="teal" />
          {attractionCost}: {attraction.price} {newShekel}
        </label>
        <br /> <br />
        <hr />
      </Container>
      <Container text>
        <Item.Group>
          <Item>
            <Item.Content>
              <Item.Header as="h2">{secondTitle}</Item.Header>
              <Item.Description>
                <strong>
                  <p>{attraction.summary}</p>
                </strong>
              </Item.Description>
            </Item.Content>
          </Item>
        </Item.Group>
        <Grid columns={3} centered container>
          <Grid.Row stretched>
            <Grid.Column width="8">
              <Image
                src={attraction.images[0] ? attraction.images[0].image : ""}
                alt="picOfAttraction"
              />
            </Grid.Column>
            <Grid.Column>
              <Image
                src={attraction.images[1] ? attraction.images[1].image : ""}
                alt="picOfAttraction"
              />
              <br />
              <Image
                src={attraction.images[2] ? attraction.images[2].image : ""}
                alt="picOfAttraction"
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
      <hr />
      <Container text>
        <Header as="h2">
          {thirdTitle}
          {isUserIsHost() && (
            <Modal
              onClose={() => setOpenCreateEventModal(false)}
              onOpen={() => setOpenCreateEventModal(true)}
              open={openCreateEventModal}
              trigger={
                <Button floated="left" color="teal">
                  {createEventText}
                </Button>
              }
            >
              <Modal.Header>Create Event</Modal.Header>
              <Modal.Content>
                <Modal.Description>
                  <CreateEvent
                    attractionId={attraction.attraction_ID}
                    attractionName={attraction.attraction_name}
                  />
                </Modal.Description>
              </Modal.Content>
              <Modal.Actions>
                <Button
                  content={close}
                  onClick={() => setOpenCreateEventModal(false)}
                  // TODO: add navigate to the same page to show new event
                />
              </Modal.Actions>
            </Modal>
          )}
        </Header>
        <AvailableAttractionList
          attractionList={attraction.available_attraction}
        />
        <br /> <br />
      </Container>
    </AttractionContext.Provider>
  );
};

export default AttractionInfo;
