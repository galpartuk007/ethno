from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

from . import views

urlpatterns = [
    path('auth/obtain-token/', obtain_jwt_token),
    path('auth/refresh-token/', refresh_jwt_token),
    path('get_attraction/', views.get_attraction),
    path('search_attraction/', views.search),
    path('make_reservation/', views.make_reservation),
    path('get_user_info', views.get_user_info),
    path('init_search_page/', views.init_search_page),
    path('create_new_attraction/', views.create_new_attraction),
    path('get_all_user_attractions/', views.get_all_user_attractions),
    path('create_new_event/', views.create_new_event),
    path('get_random_attraction/', views.get_random_attraction),
    path('register/', views.register),
    path('get_all_user_reservations/', views.get_all_user_reservations)
]
