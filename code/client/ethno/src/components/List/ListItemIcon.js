import React from "react";
import { List } from "semantic-ui-react";
import ListItemButton from "./ListItemButton";

const ListItemIcon = (props) => {
const iconName= props.type.icon ;
const iconDescription = props.type.kind;
  return (
    <List.Item onClick={props.onClick}>
        <List.Content>
            <i className={iconName} style={{ color: "grey" }}></i>
            <List.Header>{iconDescription}</List.Header>
        </List.Content>
    </List.Item>
  );
};

export default ListItemIcon;
